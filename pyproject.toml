[build-system]
requires = ["setuptools>=64", "setuptools_scm>=8"]
build-backend = "setuptools.build_meta"

[tool.setuptools.packages]
find = {}

[tool.setuptools_scm]

[project]
dynamic = ["version"]
name = "dkist-processing-common"
requires-python = ">=3.11"
description = "Common task classes used by the DKIST science data processing pipelines"
readme = "README.rst"
authors = [ {name = "NSO / AURA", email = "dkistdc@nso.edu"} ]
license = {text = "BSD-3-Clause"}
classifiers = [
  "Programming Language :: Python",
  "Programming Language :: Python :: 3",
  "Programming Language :: Python :: 3.11",
]
dependencies = [
  "asdf >= 3.5.0, <4.0.0",
  "astropy  >= 6.0.0, < 7.0.0",
  "dkist-fits-specifications >= 4.0.0, <5.0",
  "dkist-header-validator >= 5.0.0, <6.0",
  "dkist-processing-core == 5.1.0",
  "dkist-processing-pac >= 3.1, <4.0",
  "dkist-service-configuration >= 2.0.2, <3.0",
  "dkist-spectral-lines >= 3.0.0, <4.0",
  "globus-sdk >= 3.12.0",
  "gqlclient == 1.2.3",
  "sqids == 0.5.1",
  "matplotlib >= 3.4",
  "moviepy >= 2.0.0",
  "numpy >= 1.20.2",
  "object-clerk == 0.1.1",
  "pandas >= 1.4.2",
  "pillow >= 10.2.0",
  "pydantic >= 2.0",
  "redis == 4.6.0",
  "requests >= 2.23",
  "scipy >= 1.15.1",
  "sunpy >= 3.0.0",
  "talus == 1.1.0",
]

[project.urls]
Homepage = "https://nso.edu/dkist/data-center/"
Repository = "https://bitbucket.org/dkistdc/dkist-processing-common/"
Documentation = "https://docs.dkist.nso.edu/projects/common"
Help = "https://nso.atlassian.net/servicedesk/customer/portal/5"

[project.optional-dependencies]
test = [
  "pytest",
  "pytest-xdist",
  "pytest-cov",
  "pytest-mock",
  "hypothesis",
  "towncrier",
  "dkist-data-simulator >= 5.0.0",
  "dkist-processing-common[inventory]",
  "dkist-processing-common[asdf]",
  "dkist-processing-common[quality]",
]
docs = [
  "sphinx",
  "sphinx-astropy",
  "sphinx-changelog",
  "sphinx-autoapi != 3.1.0",
  "pytest",
  "towncrier < 22.12.0",
  "dkist-sphinx-theme",
]
inventory = [
  "dkist-inventory >= 1.6.0, <2.0",
]
asdf = [
  "dkist-inventory[asdf] >= 1.6.0, <2.0",
]
quality = [
  "dkist-quality >= 1.2.1, <2.0",
]

[tool.pytest.ini_options]
markers = [
  "development: For tests that can only be run while developing with a sidecar proxy (as opposed to in bitbucket pipelines)",
  "flaky: For tests that sporadically fail",
]

[tool.coverage.run]
omit = [
    "dkist_processing_common/tests/*",
    "dkist_processing_common/__init__.py",
]

[tool.coverage.report]
show_missing = true

[tool.towncrier]
    package = "dkist_processing_common"
    filename = "CHANGELOG.rst"
    directory = "changelog/"
    issue_format = "`#{issue} <https://bitbucket.org/dkistdc/dkist-processing-common/pull-requests/{issue}>`__"
    title_format = "{version} ({project_date})"
    ignore = [".gitempty"]

[[tool.towncrier.type]]
      directory = "feature"
      name = "Features"
      showcontent = true

[[tool.towncrier.type]]
      directory = "bugfix"
      name = "Bugfixes"
      showcontent = true

[[tool.towncrier.type]]
      directory = "removal"
      name = "Removals"
      showcontent = true

[[tool.towncrier.type]]
      directory = "misc"
      name = "Misc"
      showcontent = true

[[tool.towncrier.type]]
      directory = "doc"
      name = "Documentation"
      showcontent = true
