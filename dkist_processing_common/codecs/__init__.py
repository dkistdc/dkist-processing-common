"""
Encoders and decoders for `WorkflowTaskBase`'s `write` and `read` methods.

All encoders should return `bytes` and all decoders should accept `Paths`.
"""
